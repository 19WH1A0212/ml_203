# ML_203

**STOCK PRICE PREDICTION**

Stock market prediction is an attempt to predict the future value of an individual stock or a particular sector of market.
Stock market prediction aims to determine the future movement of the stock value of a financial exchange.The accurate prediction of share price movement will lead to more profit investors can make.

Stock market predictions are divided into two main categories:

**Technical Analysis**: Technical analysis attempts to predict future price movements,providing trades with the information needed to make profit.

**Fundamental Analysis**: The fundamental analysis involves techniques such as calculate the weigh tof financial indicators evaluating and selecting stocks and forecasting stock price.
